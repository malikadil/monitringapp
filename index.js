const mongoose = require("mongoose");
const app = require("./app");
const config = require("./config/config");
const logger = require("./config/logger");
const Sentry = require("@sentry/node");

// Set PORT from .env value or set 3000 as default
const PORT = process.env.PORT || 3000;

let server;
mongoose.set("strictQuery", false);

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(config.mongoose.uri);
    logger.info(`MongoDB Connected`); // : ${conn.connection.host}`);
  } catch (error) {
    Sentry.captureException(error);
    logger.warning(error);
    process.exit(1);
  }
};

//Connect to the database before listening
connectDB().then(() => {
  server = app.listen(PORT, () => {
    logger.info(`Listening to port ${config.port}` + ` for requests and Process id  ${process.pid}`);
  });
});

// mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
//   logger.info("Connected to MongoDB");
//   if (config.env !== "test") {
//     server = app.listen(config.port, () => {
//       logger.info(`Listening to port ${config.port}`);
//     });
//   }
// });

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info("Server closed");
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on("uncaughtException", unexpectedErrorHandler);
process.on("unhandledRejection", unexpectedErrorHandler);

process.on("SIGTERM", () => {
  logger.info("SIGTERM received");
  if (server) {
    server.close();
  }
});

module.exports = app;
