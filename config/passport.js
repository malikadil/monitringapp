//aws v3 code
const { SecretsManagerClient, GetSecretValueCommand } = require("@aws-sdk/client-secrets-manager");

const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const config = require("./config");
const { tokenTypes } = require("./tokens");
const User = require("../user/models/user.model");  
const Sentry = require("@sentry/node"); 
// Define a JWT strategy for Passport.js
//config jwtOption with secret key from AWS secrets Manager
// aws v3 code
const jwtOptions = {
  secretOrKeyProvider: async (req, token, done) => {
    const client = new SecretsManagerClient({
      credentials:{
        accessKeyId: process.env.APP_AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.APP_AWS_SECRET_ACCESS_KEY
      },
      region: process.env.APP_AWS_REGION
    });
    try {
      const command = new GetSecretValueCommand({ SecretId: process.env.APP_AWS_SECRET_NAME });
      const data = await client.send(command);
      const secret = JSON.parse(data.SecretString);
      done(null, secret.JWT_SECRET_KEY);
    } catch (err) {
      Sentry.captureException(error);
      done(err);
    }
  },
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};


const jwtVerify = async (payload, done) => {
  try {
    if (payload.type !== tokenTypes.ACCESS) {
      throw new Error("Invalid token type");
    }
    const user = await User.findById(payload.sub);
    if (!user) {
      return done(null, false);
    }
    done(null, user);
  } catch (error) {
    Sentry.captureException(error);
    done(error, false);
  }
};

const jwtStrategy = new JwtStrategy(jwtOptions, jwtVerify);

module.exports = {
  jwtStrategy
};
