const dotenv = require("dotenv");
const path = require("path");
const {getSecretKey} = require("../utils/nodeCache");

dotenv.config({ path: path.join(__dirname, "../.env") });

const envVars = process.env;

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  mongoose: {
    uri: envVars.MONGODB_URI,
  },
  sentrydsn: envVars.SENTRY_DSN,
  jwt: { 
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: 10,
  },
};
