const express = require("express");
const authRoute = require("../user/routes/auth.route");
const selectorRoute = require("../user/routes/selector.route");
const productRoute = require("../user/routes/product.route");
const userRoute = require("../user/routes/user.route");
const sellerRoute = require("../user/routes/seller.route");
const categoryRoute = require("../user/routes/category.route");
const statisticsRoute = require("../user/routes/statistics.route");
const appsettingsRoute = require("../user/routes/appsettings.route");

const router = express.Router();

const defaultRoutes = [
  {
    path: "/",
    route: authRoute,
  },
  {
    path: "/selectors",
    route: selectorRoute,
  },
  {
    path: "/products",
    route: productRoute,
  },
  {
    path: "/users",
    route: userRoute,
  },
  {
    path: "/sellers",
    route: sellerRoute,
  },
  {
    path: "/categories",
    route: categoryRoute,
  },
  {
    path: "/statistics",
    route: statisticsRoute,
  },
  {
    path: "/appsettings",
    route: appsettingsRoute,
  }
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

module.exports = router;
