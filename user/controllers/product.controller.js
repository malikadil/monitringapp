const httpStatus = require("http-status");
const productService = require("../services/product.service");
const Sentry = require('@sentry/node');

const catchAsync = require("../../utils/catchAsync");

const getProduct = catchAsync(async (req, res, next) => {
  try {
    const result = await productService.getProduct(req.user._id, req.params.id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);

  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const deleteProduct = catchAsync(async (req, res, next) => {
  try {
    const result = await productService.deleteProduct(
      req.user._id,
      req.params.id
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.NO_CONTENT).send(result);

  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const updateProduct = catchAsync(async (req, res, next) => {
  try {
    const result = await productService.updateProduct(
      req.user._id,
      req.params.id,
      req.body
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);

  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const getProducts = catchAsync(async (req, res, next) => {
  try {
    const result = await productService.getProducts(req.user._id, req.query);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);

  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const getAvailableFilters = catchAsync(async (req, res, next) => {
  try {
    const result = await productService.getAvailableFilters(
      req.user._id,
      req.query
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);

  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const getBrands = catchAsync(async (req, res, next) => {
  try {
    const result = await productService.getBrands(req.user._id, req.query);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);

  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
})

module.exports = {
  getProduct,
  deleteProduct,
  updateProduct,
  getProducts,
  getAvailableFilters,
  getBrands,
};
