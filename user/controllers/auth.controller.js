const httpStatus = require("http-status");
const authService = require("../services/auth.service");
const { validationResult } = require("express-validator");
const catchAsync = require("../../utils/catchAsync");
const Sentry = require('@sentry/node');

const login = catchAsync(async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(httpStatus.BAD_REQUEST).json({ errors: errors.array() });
    }

    const result = await authService.login(req);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res.status(result.status).json({ status: result.status, message: result.message });
    }

    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const refreshTokens = catchAsync(async (req, res, next) => {
  try {
    const result = await authService.refreshTokens(req);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

module.exports = {
  login,
  refreshTokens,
};
