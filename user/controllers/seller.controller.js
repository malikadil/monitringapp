const httpStatus = require("http-status");
const sellerService = require("../services/seller.service");
const Sentry = require('@sentry/node');

const catchAsync = require("../../utils/catchAsync");

const getProductSellers = catchAsync(async (req, res, next) => {
  try {
    const result = await sellerService.getProductSellers(req.user._id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const getProductSeller = catchAsync(async (req, res, next) => {
  try {
    const result = await sellerService.getProductSeller(
      req.user._id,
      req.params.id
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const updateProductSeller = catchAsync(async (req, res, next) => {
  try {
    const result = await sellerService.updateProductSeller(
      req.user._id,
      req.params.id,
      req.body,
      req.file
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const deleteSeller = catchAsync(async (req, res, next) => {
  try {
    const result = await sellerService.deleteSeller(req.user._id, req.params.id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.NO_CONTENT).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const createSeller = catchAsync(async (req, res, next) => {
  try {
    const result = await sellerService.createSeller(
      req.user._id,
      req.body,
      req.file
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.CREATED).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

module.exports = {
  getProductSellers,
  getProductSeller,
  updateProductSeller,
  deleteSeller,
  createSeller,
};
