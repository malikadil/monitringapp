const httpStatus = require('http-status');
const appSettingService = require('../services/appsettings.service');
const Sentry = require('@sentry/node');
const catchAsync = require("../../utils/catchAsync");

// GET / - Get all appSettings
const getAllAppSettings = catchAsync(async (req, res, next) => {
    try {
        const result = await appSettingService.getAllAppSettings(
            req.user._id
        );
        if (result && result.error) {
            Sentry.captureMessage(result.message); // Capture error message in Sentry
            return res
                .status(result.status)
                .json({ status: result.status, message: result.message });
        }
        res.status(httpStatus.OK).json(result);
    } catch (error) {
        Sentry.captureException(error); // Capture and report the exception to Sentry
        next(error);
    }
});

// GET /:operation - Get appSettings by operation
const getAppSettingsByOperation = catchAsync(async (req, res, next) => {
    try {
        const result = await appSettingService.getAppSettingsByOperation(
            req.user._id,
            req.params.operation,
            req.query
        );
        if (result && result.error) {
            Sentry.captureMessage(result.message); // Capture error message in Sentry
            return res
                .status(result.status)
                .json({ status: result.status, message: result.message });
        }
        res.status(httpStatus.OK).send(result);
    } catch (error) {
        Sentry.captureException(error); // Capture and report the exception to Sentry
        next(error);
    }
});

// PUT /:operation - Update appSettings by operation
const updateAppSettingsByOperation = catchAsync(async (req, res, next) => {
    const { operation } = req.params;
    const { service } = req.query;
    const updatedAppSettings = req.body;
    const userId = req.user._id;


    try {
        if (req.user.userLevel === 1 && req.user.isActive === 1) {
            const result = await appSettingService.updateAppSettingsByOperation(
                userId,
                operation,
                service,
                updatedAppSettings
            );
            if (result && result.error) {
                Sentry.captureMessage(result.message); // Capture error message in Sentry
                return res
                    .status(result.status)
                    .json({ status: result.status, message: result.message });
            }
            res.status(httpStatus.OK).send(result);
        } else {
            res.status(httpStatus.FORBIDDEN).json({ error: 'Forbidden' });
        }

    } catch (error) {
        Sentry.captureException(error); // Capture and report the exception to Sentry
        next(error);
    }
});

module.exports = {
    getAllAppSettings,
    getAppSettingsByOperation,
    updateAppSettingsByOperation,
};
