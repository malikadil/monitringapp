const httpStatus = require("http-status");
const selectorService = require("../services/selector.service");
const Sentry = require('@sentry/node');

const catchAsync = require("../../utils/catchAsync");

const getSelectors = catchAsync(async (req, res, next) => {
  try {

    const result = await selectorService.getSelectors(req.user._id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const deleteSelector = catchAsync(async (req, res, next) => {
  try {

    const result = await selectorService.deleteSelector(
      req.user._id,
      req.params.id
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.NO_CONTENT).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const getSelector = catchAsync(async (req, res, next) => {
  try {

    const result = await selectorService.getSelector(req.user._id, req.params.id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const createSelector = catchAsync(async (req, res, next) => {
  try {

    const result = await selectorService.createSelector(req.user._id, req.body);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.CREATED).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const updateSelector = catchAsync(async (req, res, next) => {
  try {

    const result = await selectorService.updateSelector(
      req.user._id,
      req.params.id,
      req.body
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

module.exports = {
  getSelectors,
  deleteSelector,
  getSelector,
  createSelector,
  updateSelector,
};
