const httpStatus = require("http-status");
const userService = require("../services/user.service");
const Sentry = require('@sentry/node');

const catchAsync = require("../../utils/catchAsync");

const getUsers = catchAsync(async (req, res, next) => {
  try {

    const result = await userService.getUsers(req.user._id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const getUser = catchAsync(async (req, res, next) => {
  try {

    const result = await userService.getUser(req.user._id, req.params.id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const createUser = catchAsync(async (req, res, next) => {
  try {

    const result = await userService.createUser(req.user._id, req.body);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.CREATED).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const updateUser = catchAsync(async (req, res, next) => {
  try {

    const result = await userService.updateUser(
      req.user._id,
      req.params.id,
      req.body
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const deleteUser = catchAsync(async (req, res, next) => {
  try {

    const result = await userService.deleteUser(req.user._id, req.params.id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.NO_CONTENT).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

// const renewKeys = catchAsync(async (req, res, next) => {
//   const result = await userService.renewKeys(
//     req.user._id,
//     req.params.id,
//     req.body.operation
//   );
//   if (result && result.error)
//     return res
//       .status(result.status)
//       .json({ status: result.status, message: result.message });
//   res.status(httpStatus.OK).send(result);
// });

module.exports = {
  getUsers,
  deleteUser,
  getUser,
  createUser,
  updateUser,
  // renewKeys,
};
