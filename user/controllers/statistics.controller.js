const httpStatus = require("http-status");
const statisticsService = require("../services/statistics.service");
const Sentry = require('@sentry/node');

const catchAsync = require("../../utils/catchAsync");

const getStatistics = catchAsync(async (req, res, next) => {
  try {

    const result = await statisticsService.getStatistics(req.user._id, req.query);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});
const getStatisticsBySellerId = catchAsync(async (req, res, next) => {
  try {

    const result = await statisticsService.getStatisticsBySellerId(
      req.user._id,
      req.params.id
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

module.exports = {
  getStatistics,
  getStatisticsBySellerId,
};
