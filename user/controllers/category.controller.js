const httpStatus = require("http-status");
const categoryService = require("../services/category.service");
const Sentry = require('@sentry/node');

const catchAsync = require("../../utils/catchAsync");

const getCategories = catchAsync(async (req, res, next) => {
  try {
    const result = await categoryService.getCategories(req.user._id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);

  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const deleteCategory = catchAsync(async (req, res, next) => {
  try {
    const result = await categoryService.deleteCategory(
      req.user._id,
      req.params.id
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.NO_CONTENT).send(result);

  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const getCategory = catchAsync(async (req, res, next) => {
  try {
    const result = await categoryService.getCategory(req.user._id, req.params.id);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const createCategory = catchAsync(async (req, res, next) => {
  try {
    const result = await categoryService.createCategory(req.user._id, req.body);
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.CREATED).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

const updateCategory = catchAsync(async (req, res, next) => {
  try {
    const result = await categoryService.updateCategory(
      req.user._id,
      req.params.id,
      req.body
    );
    if (result && result.error) {
      Sentry.captureMessage(result.message); // Capture error message in Sentry
      return res
        .status(result.status)
        .json({ status: result.status, message: result.message });
    }
    res.status(httpStatus.OK).send(result);
  } catch (error) {
    Sentry.captureException(error); // Capture exception in Sentry
    next(error);
  }
});

module.exports = {
  getCategories,
  deleteCategory,
  getCategory,
  createCategory,
  updateCategory,
};
