const express = require("express");
const sellerController = require("../controllers/seller.controller");
const auth = require("../../middlewares/auth");
const upload = require("../../middlewares/upload");

const router = express.Router();

router.get("/", auth(), sellerController.getProductSellers);

router.get("/:id", auth(), sellerController.getProductSeller);
router.put("/:id", auth(), upload.single("sellerLogo"), sellerController.updateProductSeller);
router.delete("/:id", auth(), sellerController.deleteSeller);
router.post("/", auth(), upload.single("sellerLogo"), sellerController.createSeller);

module.exports = router;
