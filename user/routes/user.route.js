const express = require("express");
const userController = require("../controllers/user.controller");
const auth = require("../../middlewares/auth");

const router = express.Router();

router.get("/", auth(), userController.getUsers);
router.get("/:id", auth(), userController.getUser);
router.post("/", auth(), userController.createUser);
router.put("/:id", auth(), userController.updateUser);
router.delete("/:id", auth(), userController.deleteUser);
// router.post("/:id", auth(), userController.renewKeys);

module.exports = router;
