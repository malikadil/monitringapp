const express = require("express");
const categoryController = require("../controllers/category.controller");
const auth = require("../../middlewares/auth");

const router = express.Router();

router.get("/categories", auth(), categoryController.getCategories);
router.get("/categories/:id", auth(), categoryController.getCategory);
router.delete("/categories/:id", auth(), categoryController.deleteCategory);
router.post("/categories", auth(), categoryController.createCategory);
router.put("/categories/:id", auth(), categoryController.updateCategory);

module.exports = router;
