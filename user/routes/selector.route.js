const express = require("express");
const selectorController = require("../controllers/selector.controller");
const auth = require("../../middlewares/auth");

const router = express.Router();

router.get("/", auth(), selectorController.getSelectors);
router.get("/:id", auth(), selectorController.getSelector);
router.delete("/:id", auth(), selectorController.deleteSelector);
router.post("/", auth(), selectorController.createSelector);
router.put("/:id", auth(), selectorController.updateSelector);

module.exports = router;
