const express = require('express');
const router = express.Router();
const appSettingsController = require('../controllers/appsettings.controller');
const auth = require("../../middlewares/auth");

// GET / - Get all appSettings
router.get('/', auth(), appSettingsController.getAllAppSettings);

// GET /:operation - Get appSettings by operation
router.get('/:operation', auth(), appSettingsController.getAppSettingsByOperation);

// PUT /:operation - Update appSettings by operation
router.put('/:operation', auth(), appSettingsController.updateAppSettingsByOperation);

module.exports = router;