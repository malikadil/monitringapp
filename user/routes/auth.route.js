const express = require("express");
const authController = require("../controllers/auth.controller");
const auth = require("../../middlewares/auth");
const rateLimiter = require("../../middlewares/rateLimiter");
const { check } = require("express-validator");

const router = express.Router();

router.post(
  "/auth",
  rateLimiter({ windowInMinutes: 1, maxRequests: 15 }),
  [check("apiKey").trim().escape(), check("secretKey").trim().escape()],
  authController.login
);

router.post(
  "/auth/refresh",
  rateLimiter({ windowInMinutes: 5, maxRequests: 5 }),
  authController.refreshTokens
);

module.exports = router;
