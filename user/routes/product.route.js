const express = require("express");
const productController = require("../controllers/product.controller");
const auth = require("../../middlewares/auth");

const router = express.Router();

// use DELETE and product id of product to remove the product from db
router.delete("/:id", auth(), productController.deleteProduct);
// use PUT and product id and send whole product JSON structure as body to update the product
router.put("/:id", auth(), productController.updateProduct);
// use GET without any id as Param and send search, sellerId(optional), categoryId(optional), includeVariants(optional
// to find products related to search parameters)
router.get("/", auth(), productController.getProducts);
//
router.get("/filters/", auth(), productController.getAvailableFilters);
//

router.get("/brands", auth(), productController.getBrands);

// use GET and product id to find a product by using id
router.get("/:id", auth(), productController.getProduct);


module.exports = router;
