const express = require("express");
const statisticsController = require("../controllers/statistics.controller");
const auth = require("../../middlewares/auth");

const router = express.Router();

router.get("/", auth(), statisticsController.getStatistics);
router.get("/:id", auth(), statisticsController.getStatisticsBySellerId);

module.exports = router;
