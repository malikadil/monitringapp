const mongoose = require("mongoose");

const websiteSelectorSchema = mongoose.Schema(
  {
    selectorsOwner: {
      type: String,
    },
    Xpaths: {
      type: Object,
    },
  },
  { versionKey: false }
);

/**
 * @typedef WebsiteSelector
 */
const WebsiteSelector = mongoose.model(
  "WebsiteSelector",
  websiteSelectorSchema,
  "websitesSelectors"
);

module.exports = WebsiteSelector;
