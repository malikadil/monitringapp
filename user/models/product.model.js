const mongoose = require("mongoose");

const productSchema = mongoose.Schema(
  {
    productTitle: {
      type: String,
    },
    productDescription: {
      type: String,
    },
    productLink: {
      type: String,
    },
    imageLink: [
      {
        type: String,
      },
    ],
    productLocalId: {
      type: String,
    },
    productBrand: {
      type: String,
    },
    sellerName: {
      type: String,
    },
    stockStatus: {
      stockStatus: {
        type: Number,
      },
      stockCount: {
        type: Number,
      },
    },
    userRating: {
      ratingStars: {
        type: String,
      },
      ratingCount: {
        type: Number,
      },
    },
    productUPC: {
      type: String,
    },
    productModel: {
      type: String,
    },
    productVariants: {
      type: Object,
    },
    sellerId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    productCategoryId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    productProcessTime: {
      type: Number,
    },
    productProcessSize: {
      type: Number,
    },
    updateStatus: {
      type: Number,
    },
    lastUpdate: {
      type: Number,
    },
    aggregationId: {
      type: Number,
    },
  },
  { versionKey: false }
);

/**
 * @typedef Product
 */
const Product = mongoose.model("Product", productSchema, "products");

module.exports = Product;
