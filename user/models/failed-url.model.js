const mongoose = require("mongoose");

const failedUrlsSchema = mongoose.Schema(
  {
    url: {
      type: String,
    },
    categoryId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    urlType: {
      type: String,
    },
    sellerId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    productLocalId: {
      type: String,
    },
  },
  { versionKey: false }
);

/**
 * @typedef FailedUrls
 */
const FailedUrls = mongoose.model("FailedUrls", failedUrlsSchema, "failedUrls");

module.exports = FailedUrls;
