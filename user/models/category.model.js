const mongoose = require("mongoose");

const productCategorySchema = mongoose.Schema(
  {
    categoryName: {
      type: String,
    },
    isRoot: {
      type: Number,
    },
    parentId: {
      type: mongoose.Schema.Types.ObjectId,
    },
  },
  { versionKey: false }
);

/**
 * @typedef ProductCategory
 */
const ProductCategory = mongoose.model(
  "ProductCategory",
  productCategorySchema,
  "productCategory"
);

module.exports = ProductCategory;
