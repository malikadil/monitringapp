const mongoose = require("mongoose");

// Define the sub-document schema for appProxies
const appProxySchema = new mongoose.Schema({
  endPoint: { type: String },
  isActive: Number,
  servicePriority: Number,
  credentials: {
    type: Object,
  },
  sellers: [{ type: mongoose.Schema.Types.ObjectId }],
},{
  _id: false // Disable automatic creation of _id field for this nested schema
});

const appSettingSchema = new mongoose.Schema({
  updatePeriod: {
    type: Number,
  },
  findPeriod: {
    type: Number,
  },
  processType: {
    type: Number,
  },
  requestInterval: {
    type: Number,
  },
  aggregatePeriod: {
    type: Number,
  },
  appProxies: {
    type: Map,
    of: appProxySchema,
  },
  sellersCredentials: {
    type: Object,
  },
  updateTask: {
    type: Object,
  },
});

const AppSetting = mongoose.model(
  "AppSetting",
  appSettingSchema,
  "appSettings"
);

module.exports = AppSetting;
