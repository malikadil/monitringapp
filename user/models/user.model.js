const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const userSchema = mongoose.Schema(
  {
    userName: {
      type: String,
    },
    userLevel: {
      type: Number,
    },
    apiKey: {
      type: String,
    },
    secretKey: {
      type: String,
      select: false,
    },
    deviceDetails: {
      ip: { type: String },
      country: String,
      state: String,
      city: String,
      latitude: String,
      longitude: String,
      internetProvider: String,
      localTimeZone: String,
    },
    isActive: {
      type: Number,
    },
    lastRequestTime: {
      type: Number,
    },
  },
  { versionKey: false }
);

userSchema.methods.compareSecretKey = async function (enteredSecretKey) {
  return await bcrypt.compare(enteredSecretKey, this.secretKey);
};

/**
 * @typedef User
 */
const User = mongoose.model("MonitoringUser", userSchema, "monitoringUsers");

// User.syncIndexes();

module.exports = User;
