const mongoose = require("mongoose");

const productProcessingAddressesSchema = mongoose.Schema(
  {
    categoryAddress: {
      type: Array,
    },
    localCategoryId: {
      type: Array,
    },
    sellerName: {
      type: String,
    },
    sellerId: {
      type: mongoose.Schema.Types.ObjectId,
    },
  },
  { versionKey: false }
);

/**
 * @typedef ProductProcessingAddresses
 */
const ProductProcessingAddresses = mongoose.model(
  "ProductProcessingAddresses",
  productProcessingAddressesSchema,
  "productProcessingAddresses"
);

module.exports = ProductProcessingAddresses;
