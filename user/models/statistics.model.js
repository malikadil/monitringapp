const mongoose = require("mongoose");

const processStatisticsSchema = mongoose.Schema(
  {
    sellerId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    processType: {
      type: String,
    },
    forceStop: {
      type: Boolean,
    },
    totalProductsProcessed: {
      type: Number,
    },
    totalProductsVariantsProcessed: {
      type: Number,
    },
    productsSavedToDb: {
      type: Number,
    },
    duplicatedProducts: {
      type: Number,
    },
    noPriceIgnore: {
      type: Number,
    },
    totalFailedUrls: {
      type: Number,
    },
    documentUpdatedCount: {
      type: Number,
    },
    newSavedPrice: {
      type: Number,
    },
    existingPriceUpdate: {
      type: Number,
    },
    documentNonUpdateCount: {
      type: Number,
    },
    newDetectedVariants: {
      type: Number,
    },
    invalidProductDeleted: {
      type: Number,
    },
    processedGroups: {
      type: Object,
    },
    processStartTime: {
      type: Number,
    },
    processEndTime: {
      type: Number,
    },
    totalProcessTime: {
      type: Number,
    },
  },
  { versionKey: false }
);

/**
 * @typedef ProcessStatistics
 */
const ProcessStatistics = mongoose.model(
  "ProcessStatistics",
  processStatisticsSchema,
  "processStatistics"
);

module.exports = ProcessStatistics;
