const mongoose = require("mongoose");

const productSellerSchema = mongoose.Schema(
  {
    sellerName: {
      type: String,
    },
    sellerLogo: {
      type: String, // Add this field to store the base64 logo string
    },
    absoluteProxy: {
      type: Number,
    },
    insertProcessStatus: {
      type: Number,
    },
    updateProcessStatus: {
      type: Number,
    },
    insertForceStop: {
      type: Number,
    },
    updateForceStop: {
      type: Number,
    },
    insertIsActive: {
      type: Number,
    },
    updateIsActive: {
      type: Number,
    },

    updatedAt: {
      type: Number,
    },
  },
  { versionKey: false }
);

/**
 * @typedef ProductSeller
 */
const ProductSeller = mongoose.model(
  "ProductSeller",
  productSellerSchema,
  "productSellers"
);

module.exports = ProductSeller;
