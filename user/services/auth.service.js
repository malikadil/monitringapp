const httpStatus = require("http-status");
const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const ApiError = require("../../utils/ApiError");
const tokenService = require("./token.service");
const microtime = require("microtime");
const ProductSeller = require("../models/product-seller.model");
const ProductCategory = require("../models/category.model");
const request = require("request");
const { tokenTypes } = require("../../config/tokens");
const config = require("../../config/config"); 
const { getJwtSecret } = require("../../utils/awsManager");
const { setSecretKey, getSecretKey } = require("../../utils/nodeCache");
const Sentry = require("@sentry/node");

const handleDeviceDetails = async (req, userId) => {
  return new Promise(async (resolve, reject) => {
    // Updated code to get the correct IP address
    const ipList = req.headers["x-forwarded-for"]
      ? req.headers["x-forwarded-for"].split(",")
      : [];
    const ipAddress =
      ipList.length > 0
        ? ipList[0].trim()
        : req.ip || req.connection.remoteAddress;

    request(
      `http://ip-api.com/json/${ipAddress}`,
      { json: true },
      async (err, response, body) => {
        if (err) {
          console.error("Error fetching device details:", err);
          reject({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: "Error fetching device details",
            error: true,
          });
        } else {
          const deviceDetails = {
            ip: ipAddress,
            country: body.country,
            state: body.regionName,
            city: body.city,
            latitude: body.lat,
            longitude: body.lon,
            internetProvider: body.isp,
            localTimeZone: body.timezone,
          };

          await User.findByIdAndUpdate(
            userId,
            { deviceDetails: deviceDetails },
            { new: true, runValidators: true, useFindAndModify: false }
          );
          resolve();
        }
      }
    );
  });
};

const login = async (req) => {
  try {
    const body = req.body;
    let { apiKey, secretKey } = body;
    if (!apiKey) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "API key is required",
        error: true,
      };
    }
    if (!secretKey) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Secret key is required",
        error: true,
      };
    }
    const user = await User.findOne({ apiKey }).select("+secretKey");

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    const isCorrectSecretKey = await user.compareSecretKey(secretKey);

    if (!user || !isCorrectSecretKey) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Save Device details
    await handleDeviceDetails(req, user._id);

    // get secret from node-cache
    let secret = getSecretKey();
    if(secret == null || secret == undefined || secret == ""){ 
      // secret key is expired , get new key from aws and store it on node-cache
      secret = await getJwtSecret();
      setSecretKey(secret);
    }
    const tokens = await tokenService.generateAuthTokens(user, secret);

    return {
      ...tokens,
      userId: user._id,
      status: httpStatus.OK,
      jwtSecret: secret, 
      serverTime: new Date().toISOString(),
      message: "Success",
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// Refresh token handler
const refreshTokens = async (req) => {
  try {
    const refreshToken = req.body.refreshToken;
    if (!refreshToken) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Refresh token is required",
        error: true,
      };
    }  
    
    // get secret from node-cache
    let secret = getSecretKey();
    if(secret == null || secret == undefined || secret == ""){
      secret = await getJwtSecret();
      setSecretKey(secret);
    }

    // Verify token
    const decodedRefreshToken = tokenService.verifyToken(refreshToken, secret);
    if (
      !decodedRefreshToken ||
      decodedRefreshToken.type !== tokenTypes.REFRESH
    ) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // Find user by token
    const user = await User.findById(decodedRefreshToken.sub);
    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // Generate new tokens
    const newTokens = await tokenService.generateAuthTokens(user, secret);

    return {
      ...newTokens,
      userId: user._id,
      status: httpStatus.OK,
      message: "Success",
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

module.exports = {
  login,
  refreshTokens,
};
