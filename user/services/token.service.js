const moment = require("moment");
const jwt = require("jsonwebtoken");
const config = require("../../config/config");
const { tokenTypes } = require("../../config/tokens");
const httpStatus = require("http-status"); 
const { getSecretKey, setSecretKey } = require("../../utils/nodeCache");
const { getJwtSecret } = require("../../utils/awsManager");
const Sentry = require("@sentry/node");
const generateForgotToken = async (user) => {
  const accessTokenExpires = moment().add(
    config.jwt.accessExpirationMinutes,
    "minutes"
  );
  const resetToken = generateToken(
    user._id,
    accessTokenExpires,
    tokenTypes.RESET_PASSWORD
  );

  return resetToken;
};

const generateAuthTokens = async (user, secret) => {
  // Generate Access token
  const accessTokenExpires = moment().add(
    config.jwt.accessExpirationMinutes,
    "minutes"
  ); 
  const accessToken = await generateToken(
    user._id,
    accessTokenExpires,
    tokenTypes.ACCESS,
    secret
  );

  // Generate Refresh token
  const refreshTokenExpires = moment().add(
    config.jwt.refreshExpirationDays,
    "days"
  );
  const refreshToken = generateToken(
    user._id,
    refreshTokenExpires,
    tokenTypes.REFRESH,
    secret
  );

  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.valueOf() / 1000,
    },
    // refresh: {
    //   token: refreshToken,
    //   expires: refreshTokenExpires.toDate(),
    // },
  };
};

const verifyToken = (token, secret) => {
  try {
    return jwt.verify(token, secret);
  } catch (error) {
    Sentry.captureException(error);
    return false;
  }
};

const generateToken = async (userId, expires, type, secret) => {  
  const payload = {
    sub: userId,
    iat: moment().unix(),
    exp: expires.unix(),
    type,
  };
  return jwt.sign(payload, secret);
};

module.exports = {
  generateAuthTokens,
  generateToken,
  generateForgotToken,
  verifyToken,
};
