const httpStatus = require("http-status");
const User = require("../models/user.model");
const ApiError = require("../../utils/ApiError");
const microtime = require("microtime");
const Product = require("../models/product.model");
const ProductSeller = require("../models/product-seller.model"); 
const { default: mongoose } = require("mongoose");
const ProductCategory = require("../models/category.model");
const { checkLastRequestTime } = require("../../utils/functions");
const passport = require("../../config/passport");
 const Sentry = require("@sentry/node");

// This function retrieves all products based on provided filters from the database for a given user id
const getProducts = async (userId, reqQuery) => {
  try {
    const { target, query, sellerId, categoryId, includeVariants, brand } =
      reqQuery;
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Construct an empty aggregation pipeline
    let aggregation = [];

    // Add a match stage to filter products by sellerId, if sellerId is provided
    if (sellerId) {
      const seller = await ProductSeller.findOne({ _id: sellerId });
      if (!seller) {
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }
      aggregation.push({
        $match: {
          sellerId: mongoose.Types.ObjectId(sellerId),
        },
      });
    }

    // Add a match stage to filter products by categoryId, if categoryId is provided
    if (categoryId) {
      const category = await ProductCategory.findOne({ _id: categoryId });
      if (!category) {
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }
      aggregation.push({
        $match: {
          productCategoryId: mongoose.Types.ObjectId(categoryId),
        },
      });
    }

    switch (target.trim().toLowerCase()) {
      case "localid":
        aggregation.push({
          $match: {
            productLocalId: { $regex: `${query}`, $options: "i" },
          },
        });
        break;
      case "variantid":
        aggregation.push({
          $match: {
            "productVariants.variantId": { $regex: `${query}`, $options: "i" },
          },
        });
        break;
      case "upc":
        aggregation.push({
          $match: {
            productUPC: { $regex: `${query}`, $options: "i" },
          },
        });
        break;
      case "brand":
        aggregation.push({
          $match: {
            productBrand: { $regex: `${query}`, $options: "i" },
          },
        });
        break;
      case "model":
        aggregation.push({
          $match: {
            productModel: { $regex: `${query}`, $options: "i" },
          },
        });
        break;
      case "search":
        aggregation.unshift({
          $search: {
            index: "autocomplete_product_search",
            autocomplete: {
              query: `${query}`,
              path: "productTitle",
            },
          },
        });
        // Add a match stage to filter products by Brand, if Brand is provided and Target equals `search`
        if (brand) {
          aggregation.push({
            $match: {
              productBrand: { $regex: `${brand}`, $options: "i" },
            },
          });
        }
        break;
      default:
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
    }
    if (includeVariants === "1") {
      aggregation.push(
        ...[
          {
            $facet: {
              "first-pipeline": [
                {
                  $match: {
                    productVariants: {
                      $exists: true,
                    },
                  },
                },
                {
                  $set: {
                    score: {
                      $meta: "searchScore",
                    },
                  },
                },
                {
                  $lookup: {
                    from: "products",
                    let: {
                      v_id: "$productVariants.variantId",
                    },
                    pipeline: [
                      {
                        $lookup: {
                          from: "productPriceHistory",
                          localField: "_id",
                          foreignField: "productId",
                          as: "priceHistory",
                        },
                      },
                      {
                        $unwind: {
                          path: "$priceHistory",
                          preserveNullAndEmptyArrays: true,
                        },
                      },
                      {
                        $sort: {
                          "priceHistory.priceUpdateTime": -1,
                        },
                      },
                      {
                        $group: {
                          _id: "$_id",
                          root: {
                            $first: "$$ROOT",
                          },
                          priceHistory: {
                            $push: "$priceHistory",
                          },
                        },
                      },
                      {
                        $addFields: {
                          "root.priceHistory": {
                            $slice: ["$priceHistory", 10],
                          },
                        },
                      },
                      {
                        $replaceRoot: {
                          newRoot: "$root",
                        },
                      },
                      {
                        $match: {
                          $expr: {
                            $eq: ["$productVariants.variantId", "$$v_id"],
                          },
                        },
                      },
                    ],
                    as: "relatedProducts",
                  },
                },
                {
                  $unwind: {
                    path: "$relatedProducts",
                    preserveNullAndEmptyArrays: true,
                  },
                },
                {
                  $group: {
                    _id: "$productVariants.variantId",
                    mainProduct: {
                      $first: "$$ROOT",
                    },
                    relatedProducts: {
                      $addToSet: "$relatedProducts",
                    },
                  },
                },
                {
                  $addFields: {
                    "mainProduct.relatedProducts": {
                      $filter: {
                        input: "$relatedProducts",
                        as: "product",
                        cond: {
                          $ne: ["$$product._id", "$mainProduct._id"],
                        },
                      },
                    },
                  },
                },
                {
                  $replaceRoot: {
                    newRoot: "$mainProduct",
                  },
                },
                {
                  $sort: {
                    score: -1,
                  },
                },
                {
                  $lookup: {
                    from: "productPriceHistory",
                    localField: "_id",
                    foreignField: "productId",
                    as: "priceHistory", // output array field
                  },
                },
                {
                  $unwind: {
                    path: "$priceHistory",
                    preserveNullAndEmptyArrays: true,
                  },
                },
                {
                  $sort: {
                    "priceHistory.priceUpdateTime": -1, // sort by priceUpdateTime in descending order
                  },
                },
                {
                  $group: {
                    _id: "$_id",
                    root: {
                      $first: "$$ROOT",
                    },
                    priceHistory: {
                      $push: "$priceHistory",
                    },
                  },
                },
                {
                  $addFields: {
                    "root.priceHistory": {
                      $slice: ["$priceHistory", 10], // get the last 10 price history
                    },
                  },
                },
                {
                  $replaceRoot: {
                    newRoot: "$root",
                  },
                },
              ],
              "second-pipeline": [
                {
                  $match: {
                    productVariants: {
                      $exists: false,
                    },
                  },
                },
                {
                  $lookup: {
                    from: "productPriceHistory",
                    localField: "_id",
                    foreignField: "productId",
                    as: "priceHistory", // output array field
                  },
                },
                {
                  $unwind: {
                    path: "$priceHistory",
                    preserveNullAndEmptyArrays: true,
                  },
                },
                {
                  $sort: {
                    "priceHistory.priceUpdateTime": -1, // sort by priceUpdateTime in descending order
                  },
                },
                {
                  $group: {
                    _id: "$_id",
                    root: {
                      $first: "$$ROOT",
                    },
                    priceHistory: {
                      $push: "$priceHistory",
                    },
                  },
                },
                {
                  $addFields: {
                    "root.priceHistory": {
                      $slice: ["$priceHistory", 10], // get the last 10 price history
                    },
                  },
                },
                {
                  $replaceRoot: {
                    newRoot: "$root",
                  },
                },
              ],
            },
          },
          {
            $project: {
              combinedResults: {
                $concatArrays: ["$first-pipeline", "$second-pipeline"],
              },
            },
          },
          {
            $unwind: "$combinedResults",
          },
          {
            $replaceRoot: {
              newRoot: "$combinedResults",
            },
          },
          {
            $set: {
              score: { $meta: "searchScore" },
            },
          },
          {
            $sort: {
              score: -1,
            },
          },
        ]
      );
    } else {
      aggregation.push(
        ...[
          {
            $lookup: {
              from: "productPriceHistory",
              localField: "_id",
              foreignField: "productId",
              as: "priceHistory", // output array field
            },
          },
          {
            $unwind: {
              path: "$priceHistory",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $sort: {
              "priceHistory.priceUpdateTime": -1, // sort by priceUpdateTime in descending order
            },
          },
          {
            $group: {
              _id: "$_id",
              root: {
                $first: "$$ROOT",
              },
              priceHistory: {
                $push: "$priceHistory",
              },
            },
          },
          {
            $addFields: {
              "root.priceHistory": {
                $slice: ["$priceHistory", 10], // get the last 10 price history
              },
            },
          },
          {
            $replaceRoot: {
              newRoot: "$root",
            },
          },
          {
            $set: {
              score: { $meta: "searchScore" },
            },
          },
          {
            $sort: {
              score: -1,
            },
          },
        ]
      );
    }

    let products = [];
    // Retrieve products from the database using the aggregation pipeline
    if (aggregation.length === 0) {
      // If the aggregation pipeline is empty, retrieve all products from the database
      products = await Product.find();
    } else {
      // Otherwise, execute the aggregation pipeline to retrieve filtered products
      products = await Product.aggregate(aggregation);
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the products, along with a status code and message indicating success
    console.log(products)
    return { products, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    console.log(error);
    return {
      status: httpStatus.BAD_REQUEST,
      message: "Bad request",
      error: true,
    };
  }
};

//This function retrieves all Brands, Category Names, and Seller Names of products found in the database based on the provided filters
const getAvailableFilters = async (userId, reqQuery) => {
  try {
    const { sellerId, categoryId, query  } = reqQuery;
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Construct an empty aggregation pipeline
    let aggregation = []

    // Search for provided query value
    if (query) {
      aggregation.push({
        $search: {
          index: "autocomplete_product_search",
          autocomplete: {
            query: `${query}`,
            path: "productTitle",
          },
        },
      })
    }
    else {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Add a match stage to filter products by sellerId, if sellerId is provided
    if (sellerId) {
      const seller = await ProductSeller.findOne({ _id: sellerId });
      if (!seller) {
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }
      aggregation.push({
        $match: {
          sellerId: mongoose.Types.ObjectId(sellerId),
        },
      });
    }

    // Add a match stage to filter products by categoryId, if categoryId is provided
    if (categoryId) {
      const category = await ProductCategory.findOne({ _id: categoryId });
      if (!category) {
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request (categoryId)",
          error: true,
        };
      }
      aggregation.push({
        $match: {
          productCategoryId: mongoose.Types.ObjectId(categoryId),
        },
      });
    }

    aggregation.push({
      $match : {
        $and : [
          {productBrand: {$ne: "NA"}},
          {productBrand: {$ne: ""}},
          {productBrand: {$ne: null}},
        ]
      }
    });

    aggregation.push(
      {
        $lookup: {
          from: "productCategory",
          localField: "productCategoryId",
          foreignField: "_id",
          as: "productCategoryDetails",
        },
      },
      {
        $lookup: {
          from: "productSellers",
          localField: "sellerId",
          foreignField: "_id",
          as: "productSellerDetails",
        },
      },
      {
        $addFields: {
          productCategoryDetails: {
            $arrayElemAt: [
              "$productCategoryDetails",
              0,
            ],
          },
          productSellerDetails: {
            $arrayElemAt: [
              "$productSellerDetails",
              0,
            ],
          },
        },
      }, 
      {
        $group: {
          _id: "$productBrand",
          productCount: {$sum: 1},
          brandName: {$first: "$productBrand"},
          categories: {
            $addToSet: {
              categoryName: "$productCategoryDetails.categoryName",
              categoryId: "$productCategoryId",
            },
          },
          sellers: {
            $addToSet: {
              sellerName: "$productSellerDetails.sellerName",
              sellerId: "$sellerId",
            },
          },
        },
      },
      {
        $unwind: "$sellers"
      },
      {
        $unwind: "$categories"
      },
      {
        $group:{
          _id:null,
          brands: {
            $addToSet: {
              brandName: "$brandName",
              productCount: "$productCount"
            }
          },
          categories : { $addToSet: "$categories"},
          sellers : { $addToSet: "$sellers"}
        }
      },
      {
        $project: {
          _id: 0,
        },
      });

    // Execute the aggregation pipeline to retrieve filtered Brands, Category Names, and Seller Names 
    let filters = await Product.aggregate(aggregation);

    console.log(filters[0])
    let sortedFilters = sortResults(filters[0]);
    sortedFilters.brands = uniqueBrandName(sortedFilters.brands);

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the categories, along with a status code and message indicating success
    return { sortedFilters, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    console.log(error);
    return {
      status: httpStatus.BAD_REQUEST,
      message: "Bad request",
      error: true,
    };
  }
};

// This is a function to return the product by using id of product
const getProduct = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the product with the given id in the database
    //let product = await Product.findOne({ _id: id });
    let product = await Product.aggregate([
      {
        $match: { _id: mongoose.Types.ObjectId(id) }, // match the product with the given id
      },
      {
        $lookup: {
          from: "productPriceHistory", // name of the other collection, make sure it is correct
          localField: "_id", // name of the products field
          foreignField: "productId", // name of the productPriceHistory field
          as: "priceHistory", // output array field
        },
      },
      {
        $unwind: {
          path: "$priceHistory",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $sort: {
          "priceHistory.priceUpdateTime": -1, // sort by priceUpdateTime in descending order
        },
      },
      {
        $group: {
          _id: "$_id",
          root: { $first: "$$ROOT" },
          priceHistory: { $push: "$priceHistory" },
        },
      },
      {
        $addFields: {
          "root.priceHistory": {
            $slice: ["$priceHistory", 10], // get the last 10 price history
          },
        },
      },
      {
        $replaceRoot: {
          newRoot: "$root",
        },
      },
    ]);

    // If the product is not found, return an error
    if (!product) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the categories, along with a status code and message indicating success
    return { product, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This is a function to update a product in the database
const updateProduct = async (userId, id, body) => {
  const { operation, ...productBody } = body;
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the product with the given id in the database
    let product = await Product.findOne({ _id: id });

    // If the product is not found, return an error
    if (!product) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the product if the operation is valid
    if (operation === "edit") {
      await Product.findByIdAndUpdate(id, productBody);
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.OK,
      message: "Success",
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// This is a function to delete a product from the database
const deleteProduct = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the product with the given id in the database
    let product = await Product.findOne({ _id: id });

    // If the product is not found, return an error
    if (!product) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Delete the product
    await Product.deleteOne({ _id: id });
    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.NO_CONTENT,
      message: "Deleted",
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// This function retrieves all products based on provided filters from the database for a given user id
const getBrands = async (userId, reqQuery) => {
  try {
    const { sellerId, categoryId  } = reqQuery;
     
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Construct an empty aggregation pipeline
    let aggregation = [];

    // Add a match stage to filter products by sellerId, if sellerId is provided
    if (sellerId) {
      const seller = await ProductSeller.findOne({ _id: sellerId });
      if (!seller) {
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }
      aggregation.push({
        $match: {
          sellerId: mongoose.Types.ObjectId(sellerId),
        },
      });
    }

    // Add a match stage to filter products by categoryId, if categoryId is provided
    if (categoryId) {
      const category = await ProductCategory.findOne({ _id: categoryId });
      if (!category) {
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }
      aggregation.push({
        $match: {
          productCategoryId: mongoose.Types.ObjectId(categoryId),
        },
      });
    }
    
    aggregation.push({
      $match : {
        $and : [
          {productBrand: {$ne: "NA"}},
          {productBrand: {$ne: ""}},
          {productBrand: {$ne: null}},
        ]
      }
    });

    aggregation.push({ 
      $group: {
        _id: "$productBrand",
        productCount: {$sum: 1},
        brandName: {$first:"$productBrand"},
      }, 
    });
    aggregation.push({
      $project: {
        _id: 0,
      },
    });

    let products = [];
    // execute the aggregation pipeline to retrieve filtered products
    products = await Product.aggregate(aggregation);

    let sortedFilter = sortResults({brands: products});
    let brands = uniqueBrandName(sortedFilter.brands);

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the brands
    return { brands, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    console.log(error);
    return {
      status: httpStatus.BAD_REQUEST,
      message: "Bad request",
      error: true,
    };
  }
};

function sortResults(results) {
  if (results && results.brands) {
    results.brands.sort((a, b) => {
      if(a.brandName.toLowerCase() === b.brandName.toLowerCase()) return b.productCount - a.productCount;
      return a.brandName.localeCompare(b.brandName)
    });
  }
  if (results && results.categories) {
    results.categories.sort((a, b) =>
      a.categoryName.localeCompare(b.categoryName)
    );
  }
  if (results && results.sellers) {
    results.sellers.sort((a, b) => a.sellerName.localeCompare(b.sellerName));
  }
  return results;
}

function uniqueBrandName(results) { 
    let brands = [];
    let temp = "";
    results.map((brand, index) => { 
      if (brand.brandName.toLowerCase() === temp) return;
      temp = brand.brandName.toLowerCase();
      brands.push(brand.brandName);
    }); 
    return brands; 
}

module.exports = {
  getProducts,
  getAvailableFilters,
  getProduct,
  getBrands,
  updateProduct,
  deleteProduct,
};
