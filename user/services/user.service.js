const httpStatus = require("http-status");
const User = require("../models/user.model");
const ApiError = require("../../utils/ApiError");
const microtime = require("microtime");
const crypto = require("crypto");
const bcrypt = require("bcrypt");
const { checkLastRequestTime } = require("../../utils/functions");
const Sentry = require("@sentry/node");

// This function will generate apiKey securely (Using a cryptographically secure random string)
const generateApiKey = () => {
  const randomString = crypto.randomBytes(32).toString("hex");
  return randomString;
};

// This function will generate secretkey securely
const generateSecretKey = async () => {
  const secretKey = crypto.randomBytes(64).toString("hex");
  return secretKey;
};

// This function will hash the generated keys securely (with 12 salts)
const generateKeyHash = async (key) => {
  const keyHash = await bcrypt.hash(key, 12);
  return keyHash;
};

// This function retrieves all users the database for a given user id
const getUsers = async (userId) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Retrieve all users from the database
    let users = await User.find();

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the users, along with a status code and message indicating success
    return { users, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This function retrieves user details from the database for a given user id
const getUser = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Retrieve user from the database
    let usr = await User.findOne({ _id: id });

    // If the user level is not 1 AND GET id not equal to logged-in user, return an error
    if (user.userLevel !== 1 && userId.toString() !== id.toString()) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the users, along with a status code and message indicating success
    return { user: usr, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This is a function to create a user in the database
const createUser = async (userId, body) => {
  try {
    const { operation, userName, userLevel, isActive } = body;

    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }
    let newUser;
    // Generate encrypted apiKey and secretKey
    const apiKey = generateApiKey();
    const secretKey = await generateSecretKey();

    // If the operation is valid, create user
    if (operation === "add") {
      // Hash the secretKey
      const secretKeyHash = await generateKeyHash(secretKey);

      newUser = await User.create({
        userLevel,
        userName,
        apiKey,
        secretKey: secretKeyHash,
        isActive,
      });
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.CREATED,
      message: "Created",
      id: newUser._id,
      apiKey,
      secretKey,
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// This is a function to update a user in the database
const updateUser = async (userId, id, body) => {
  try {
    // Get the operation and value from the request body
    let { operation, userName, userLevel, isActive } = body;

    // Find the user with the given user id in the database
    const logged_in_user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!logged_in_user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(logged_in_user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (logged_in_user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (logged_in_user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the user with the given id in the database
    let targetUser = await User.findOne({ _id: id });

    // If the user is not found, return an error
    if (!targetUser) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    if (operation === "edit") {
      targetUser.userName = userName;
      targetUser.userLevel = userLevel;
      targetUser.isActive = isActive;
    } else if (operation === "setActive") {
      targetUser.isActive = isActive;
    } else if (operation === "setLevel") {
      targetUser.userLevel = userLevel;
    } else if (operation === "renewKeys") {
      // Generating apiKey and hashed version of secretKey
      const apiKey = generateApiKey();
      const secretKey = await generateSecretKey();
      const secretKeyHash = await generateKeyHash(secretKey);

      // Retrieve user from the database and update it
      await User.findByIdAndUpdate(
        id,
        { apiKey, secretKey: secretKeyHash },
        { new: true, runValidators: true, useFindAndModify: false }
      );

      // Return the hashed version of apiKey and secretKey along with a status code and message indicating success
      return {
        status: httpStatus.OK,
        message: "Success",
        userID: id,
        apiKey,
        secretKey,
      };
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    logged_in_user.lastRequestTime = microtime.nowDouble();
    await logged_in_user.save();
    await targetUser.save();

    // Return a success message
    return {
      status: httpStatus.OK,
      message: "Success",
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// This is a function to delete a user from the database
const deleteUser = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const logged_in_user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!logged_in_user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(logged_in_user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (logged_in_user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (logged_in_user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the user with the given id in the database
    let targetUser = await User.findOne({ _id: id });

    // If the user is not found, return an error
    if (!targetUser) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // If the logged-in user is equal to target user, return an error
    if (userId.toString() === id.toString()) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Delete the user
    await User.deleteOne({ _id: id });
    // Update the last request time for the user
    logged_in_user.lastRequestTime = microtime.nowDouble();
    await logged_in_user.save();

    // Return a success message
    return {
      status: httpStatus.NO_CONTENT,
      message: "Deleted",
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// This is a function to regenerate apiKey and secretKey
// const renewKeys = async (userId, id, operation) => {
//   try {
//     // Find the user with the given user id in the database
//     const user = await User.findOne({ _id: userId });

//     // If the user is not found, throw an error
//     if (!user) {
//       return {
//         status: httpStatus.UNAUTHORIZED,
//         message: "Unauthorized",
//         error: true,
//       };
//     }

//     // If the user level is not 1, return an error
//     if (user.userLevel !== 1) {
//       return {
//         status: httpStatus.FORBIDDEN,
//         message: "Forbidden",
//         error: true,
//       };
//     }

//     // If the last request time was more than 5 minutes ago, throw an error
//     if (!checkLastRequestTime(user.lastRequestTime)) {
//       return {
//         status: httpStatus.UNAUTHORIZED,
//         message: "Unauthorized",
//         error: true,
//       };
//     }

//     // If the user is not active, return an error
//     if (user.isActive !== 1) {
//       return {
//         status: httpStatus.FORBIDDEN,
//         message: "Forbidden",
//         error: true,
//       };
//     }

//     // Update the last request time for the user
//     user.lastRequestTime = microtime.nowDouble();
//     await user.save();

//     // If the operation is not valid, return an error
//     if (operation !== "renewKeys") {
//       return {
//         status: httpStatus.BAD_REQUEST,
//         message: "Bad request",
//         error: true,
//       };
//     }

//     // Generating apiKey and hashed version of secretKey
//     const apiKey = generateApiKey();
//     const secretKey = await generateSecretKey();
//     const secretKeyHash = await generateKeyHash(secretKey);

//     // Retrieve user from the database and update it
//     await User.findByIdAndUpdate(
//       id,
//       { apiKey, secretKey: secretKeyHash },
//       { new: true, runValidators: true, useFindAndModify: false }
//     );

//     // Return the hashed version of apiKey and secretKey along with a status code and message indicating success
//     return {
//       status: httpStatus.OK,
//       message: "Success",
//       userID: id,
//       apiKey,
//       secretKey,
//     };
//   } catch (error) {
// Sentry.captureException(error);
//     // If an error occurs, throw it to the caller
//     throw error;
//   }
// };

module.exports = {
  getUsers,
  deleteUser,
  getUser,
  createUser,
  updateUser,
  // renewKeys,
};
