const httpStatus = require("http-status");
const User = require("../models/user.model");
const ApiError = require("../../utils/ApiError");
const microtime = require("microtime");
const WebsiteSelector = require("../models/website-selector.model");
const {checkLastRequestTime} = require("../../utils/functions");
 const Sentry = require("@sentry/node");

// This function retrieves all selectors the database for a given user id
const getSelectors = async (userId) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1 and not 2, return an error
    if (user.userLevel !== 1 && user.userLevel !== 2) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Retrieve all selectors from the database
    let selectors = await WebsiteSelector.find();

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the categories, along with a status code and message indicating success
    return { selectors, status: httpStatus.OK, message: "Success" };
  } catch (error) {
     Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This is a function to delete a product selector from the database
const deleteSelector = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the product selector with the given id in the database
    let selector = await WebsiteSelector.findOne({ _id: id });

    // If the product selector is not found, return an error
    if (!selector) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Delete the product selector
    await WebsiteSelector.deleteOne({ _id: id });
    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.NO_CONTENT,
      message: "Deleted",
    };
  } catch (error) {
     Sentry.captureException(error);
    throw error;
  }
};

// This function retrieves selector details from the database for a given user id
const getSelector = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1 and not 2, return an error
    if (user.userLevel !== 1 && user.userLevel !== 2) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Retrieve selector from the database
    let selector = await WebsiteSelector.findOne({ _id: id });

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the categories, along with a status code and message indicating success
    return { selector, status: httpStatus.OK, message: "Success" };
  } catch (error) {
     Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This is a function to create a selector in the database
const createSelector = async (userId, body) => {
  try {
    const { operation, selectorsOwner, Xpaths } = body;

    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }
    let newSelector;

    // If the operation is valid, create selector
    if (operation === "add") {
      newSelector = await WebsiteSelector.create({
        selectorsOwner,
        Xpaths,
      });
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.CREATED,
      message: "Created",
      id: newSelector._id,
    };
  } catch (error) {
     Sentry.captureException(error);
    throw error;
  }
};

// This is a function to update a selector in the database
const updateSelector = async (userId, id, body) => {
  try {
    // Get the operation and value from the request body
    let { operation, selectorsOwner, Xpaths } = body;

    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the selector with the given id in the database
    let selector = await WebsiteSelector.findOne({ _id: id });

    // If the selector is not found, return an error
    if (!selector) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    if (operation === "edit") {
      selector.selectorsOwner = selectorsOwner;
      selector.Xpaths = Xpaths;
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    await selector.save();

    // Return a success message
    return {
      status: httpStatus.OK,
      message: "Success",
    };
  } catch (error) {
     Sentry.captureException(error);
    throw error;
  }
};

module.exports = {
  getSelectors,
  deleteSelector,
  getSelector,
  createSelector,
  updateSelector,
};
