const httpStatus = require("http-status");
const User = require("../models/user.model");
const microtime = require("microtime");
const ProductSeller = require("../models/product-seller.model");
const { default: mongoose } = require("mongoose");
const Product = require("../models/product.model");
const ProductProcessingAddresses = require("../models/product-processing-address.model");
const FailedUrls = require("../models/failed-url.model");
const ProcessStatistics = require("../models/statistics.model");
const ProductCategory = require("../models/category.model");
const {checkLastRequestTime} = require("../../utils/functions");
const Sentry = require("@sentry/node");

// This function retrieves all product sellers details from the database for a given user id
const getStatistics = async (userId, query) => {
  try {
    const { sellerId } = query;
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }
    // Initialize results variable
    let results = {};

    let products = await Product.find();
    let productsWithVariants = await Product.distinct(
      "productVariants.variantId"
    );

    // Query the FailedUrls collection to find all documents with a matching sellerId field.
    let totalFailedURL = await FailedUrls.find();

    // Retrieve all categories from the database
    let categories = await ProductCategory.find();

    // Initialize variables to hold total counts and processing time/size.
    let totalItems = products.length;
    let totalProcessingTime = 0;
    let totalProcessingSize = 0;

    // For each related product, add its processing time and size to the total processing time and size.
    products.forEach((product) => {
      totalProcessingTime += product.productProcessTime;
      totalProcessingSize += product.productProcessSize;
    });

    // Create an object called results to store the calculated counts and averages.
    results = {
      totalProducts: products.length,
      totalProductsWithVariants: productsWithVariants.length,
      totalLocalCategory: categories.length,
      totalFailedURL: totalFailedURL.length,
      averageProcessingTime: totalProcessingTime / totalItems,
      averageProcessingSize: totalProcessingSize / totalItems,
    };

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the product sellers, along with a status code and message indicating success
    return { details: results, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This function retrieves all product sellers details from the database for a given user id
const getStatisticsBySellerId = async (userId, sellerId) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }
    // Initialize results variable
    let results = {};

    // Check if sellerId is provided
    if (sellerId) {
      // If yes, find the product seller by sellerId
      let productSeller = await ProductSeller.find({ _id: sellerId });

      // If the product seller is not found, return an error
      if (!productSeller) {
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }

      let products = await Product.find({ sellerId: sellerId });

      let productsWithVariants = await Product.find({
        sellerId: sellerId,
      }).distinct("productVariants.variantId");

      // Query the ProductProcessingAddresses collection to find a document with a matching sellerId field.
      let groupToProcess = await ProductProcessingAddresses.findOne({
        sellerId: sellerId,
      });

      // Query the FailedUrls collection to find all documents with a matching sellerId field.
      let totalFailedURL = await FailedUrls.find({ sellerId: sellerId });

      // Initialize variables to hold total counts and processing time/size.
      // let totalItems = 0;
      let totalItems = products.length;
      let totalProcessingTime = 0;
      let totalProcessingSize = 0;

      // For each related product, add its processing time and size to the total processing time and size.
      products.forEach((product) => {
        totalProcessingTime += product.productProcessTime;
        totalProcessingSize += product.productProcessSize;
      });

      let processStatistics = await ProcessStatistics.find({
        sellerId: sellerId,
      });
      // Create an object called results to store the calculated counts and averages.
      results = {
        totalProducts: products.length,
        totalProductsWithVariants: productsWithVariants.length,
        groupToProcess: groupToProcess
          ? groupToProcess.categoryAddress.length
          : 0,
        totalFailedURL: totalFailedURL.length,
        averageProcessingTime: totalProcessingTime / totalItems,
        averageProcessingSize: totalProcessingSize / totalItems,
        processStatistics: processStatistics,
      };
    } else {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the product sellers, along with a status code and message indicating success
    return { details: results, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};
module.exports = {
  getStatistics,
  getStatisticsBySellerId,
};
