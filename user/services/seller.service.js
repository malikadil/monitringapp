const httpStatus = require("http-status");
const User = require("../models/user.model");
const microtime = require("microtime");
const ProductSeller = require("../models/product-seller.model");
const {checkLastRequestTime} = require("../../utils/functions");
const Sentry = require("@sentry/node");

// This function retrieves all product sellers from the database for a given user id
const getProductSellers = async (userId) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }
    // Retrieve all product sellers from the database and map them to a new array with a subset of properties
    let productSellers = await ProductSeller.find();

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the product sellers, along with a status code and message indicating success
    return { productSellers, status: httpStatus.OK, message: "Success" };
  } catch (error) {
     Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This function retrieves a specific product seller from the database by id, for a given user id
const getProductSeller = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Retrieve the product seller with the given id from the database
    let productSeller = await ProductSeller.findOne({ _id: id });

    // If the product seller is not found, return an error
    if (!productSeller) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }
    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the product seller, along with a status code and message indicating success
    return {
      productSeller,
      status: httpStatus.OK,
      message: "Success",
    };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This is a function to update a product seller in the database
const updateProductSeller = async (userId, id, body, file) => {
  try {
    // Get the operation and value from the request body
    let {
      operation,
      type,
      value,
      sellerName,
      insertIsActive,
      updateIsActive,
      setProxy,
    } = body;

    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // This code restricts access to certain operations based on the user level.
    // If the user level is not 1 or 2, the operation is not allowed and an error is returned.
    // For user level 2, only certain operations are allowed, which are listed in the allowedOperationsForUserLevel2 array.
    const allowedOperationsForUserLevel2 = [
      "forceStop",
      "setProxy",
      "setActive",
    ];
    if (
      user.userLevel !== 1 &&
      (!allowedOperationsForUserLevel2.includes(operation) ||
        user.userLevel !== 2)
    ) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the product seller with the given id in the database
    let productSeller = await ProductSeller.findOne({ _id: id });

    // If the product seller is not found, return an error
    if (!productSeller) {
      console.log("1");
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // If the operation is forceStop, update the forceStop property with the given value
    if (operation === "forceStop") {
      if (type === "insert") {
        // Set the insert force stop property to the given value
        productSeller.insertForceStop = value;
      } else if (type === "update") {
        // Set the update force stop property to the given value
        productSeller.updateForceStop = value;
      } else {
        // If the type is not valid, return an error
        console.log("2");
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }
    } else if (operation === "setProxy") {
      // Set the absolute proxy property to the given value
      productSeller.absoluteProxy = value;
    } else if (operation === "setActive") {
      if (type === "insert") {
        // Set the insert is active property to the given value
        productSeller.insertIsActive = value;
      } else if (type === "update") {
        // Set the update is active property to the given value
        productSeller.updateIsActive = value;
      } else {
        // If the type is not valid, return an error
        console.log("3");
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }
    } else if (operation === "edit") {
      if (sellerName) { productSeller.sellerName = sellerName; }
      if (insertIsActive) { productSeller.insertIsActive = insertIsActive; }
      if (updateIsActive) { productSeller.updateIsActive = updateIsActive; }
      if (setProxy) { productSeller.absoluteProxy = setProxy; }
      if (file) {
        productSeller.sellerLogo = file.buffer.toString("base64");
      }
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    productSeller.updatedAt = microtime.nowDouble();
    await user.save();
    await productSeller.save();

    // Return a success message
    return {
      status: httpStatus.OK,
      message: "Success",
    };
  } catch (error) {
     Sentry.captureException(error);
    throw error;
  }
};

// This is a function to delete a product seller from the database
const deleteSeller = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the product seller with the given id in the database
    let productSeller = await ProductSeller.findOne({ _id: id });

    // If the product seller is not found, return an error
    if (!productSeller) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Delete the product seller
    await ProductSeller.deleteOne({ _id: id });
    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.NO_CONTENT,
      message: "Deleted",
    };
  } catch (error) {
     Sentry.captureException(error);
    throw error;
  }
};

// This is a function to create a product seller in the database
const createSeller = async (userId, body, file) => {
  try {
    const { operation, sellerName, setProxy, insertIsActive, updateIsActive } =
      body;

    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    let newSeller;
    // If the operation is valid, create seller
    if (operation === "add") {
      const sellerLogo = file ? file.buffer.toString("base64") : null;
      newSeller = await ProductSeller.create({
        sellerName,
        sellerLogo,
        absoluteProxy: setProxy,
        insertProcessStatus: 0,
        updateProcessStatus: 0,
        insertForceStop: 0,
        updateForceStop: 0,
        insertIsActive: insertIsActive,
        updateIsActive: updateIsActive,
        updatedAt: microtime.nowDouble(),
      });
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.CREATED,
      message: "Created",
      id: newSeller._id,
    };
  } catch (error) {
     Sentry.captureException(error);
    throw error;
  }
};

module.exports = {
  getProductSellers,
  getProductSeller,
  updateProductSeller,
  deleteSeller,
  createSeller,
};
