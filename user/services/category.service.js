const httpStatus = require("http-status");
const User = require("../models/user.model");
const microtime = require("microtime");
const ProductCategory = require("../models/category.model");
const {checkLastRequestTime } = require("../../utils/functions");
const Sentry = require("@sentry/node");

// This function retrieves all categories the database for a given user id
const getCategories = async (userId) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Retrieve all categories from the database
    let categories = await ProductCategory.find();

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the categories, along with a status code and message indicating success
    return { categories, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This is a function to delete a product category from the database
const deleteCategory = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    if (!(checkLastRequestTime(user.lastRequestTime))) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the product category with the given id in the database
    let category = await ProductCategory.findOne({ _id: id });

    // If the product category is not found, return an error
    if (!category) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Delete the product category
    await ProductCategory.deleteOne({ _id: id });
    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.NO_CONTENT,
      message: "Deleted",
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// This function retrieves category details from the database for a given user id
const getCategory = async (userId, id) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 

    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Retrieve category from the database
    let category = await ProductCategory.findOne({ _id: id });

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    // Return the categories, along with a status code and message indicating success
    return { category, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    // If an error occurs, throw it to the caller
    throw error;
  }
};

// This is a function to create a category in the database
const createCategory = async (userId, body) => {
  try {
    const { operation, categoryName, isRoot, parentId } = body;

    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }
    let newCategory;

    // If the operation is valid, create category
    if (operation === "add") {
      newCategory = await ProductCategory.create({
        categoryName,
        isRoot,
        parentId,
      });
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    // Return a success message
    return {
      status: httpStatus.CREATED,
      message: "Created",
      id: newCategory._id,
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// This is a function to update a category in the database
const updateCategory = async (userId, id, body) => {
  try {
    // Get the operation and value from the request body
    let { operation, categoryName, isRoot, parentId } = body;

    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    } 

    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // If the user level is not 1, return an error
    if (user.userLevel !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    // Find the category with the given id in the database
    let category = await ProductCategory.findOne({ _id: id });

    // If the category is not found, return an error
    if (!category) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    if (operation === "edit") {
      category.categoryName = categoryName;
      category.isRoot = isRoot;
      category.parentId = parentId;
    } else {
      // If the operation is not valid, return an error
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
    await category.save();

    // Return a success message
    return {
      status: httpStatus.OK,
      message: "Success",
    };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

module.exports = {
  getCategories,
  deleteCategory,
  getCategory,
  createCategory,
  updateCategory,
};
