const httpStatus = require("http-status");
const microtime = require("microtime");
const mongoose = require("mongoose");
const AppSetting = require("../models/appsettings.model");
const User = require("../models/user.model");
const Sentry = require('@sentry/node');

const { checkLastRequestTime } = require("../../utils/functions");

// Get all appSettings
const getAllAppSettings = async (userId) => {
  try {
    // get user
    const user = await User.findOne({ _id: userId });
    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }
    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }
    // fetch records
    const appSettings = await AppSetting.find();
    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();

    return { appSettings, status: httpStatus.OK, message: "Success" };
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// Get appSettings by operation
const getAppSettingsByOperation = async (userId, operation, reqQuery) => {
  const { service: serviceName } = reqQuery;
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, throw an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the user is not active, return an error
    if (user.isActive !== 1) {
      return {
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
        error: true,
      };
    }

    switch (operation.trim().toLowerCase()) {
      case "proxy":
        console.log("proxy");
        const appSetting = await AppSetting.findOne();
        if (appSetting.get("appProxies." + serviceName)) {
          console.log(serviceName.trim().toLowerCase());
          return {
            operation: "proxy",
            proxyService: appSetting.get("appProxies." + serviceName),
            status: httpStatus.OK,
            message: "Success",
          };
        }
        break;
      case "updatePeriod":
        return;

      default:
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
    }

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();
    await user.save();
  } catch (error) {
    Sentry.captureException(error);
    throw error;
  }
};

// Update appSettings by operation
const updateAppSettingsByOperation = async (userId, operation, service, updatedData) => {
  try {
    // Find the user with the given user id in the database
    const user = await User.findOne({ _id: userId });

    // If the user is not found, return an error
    if (!user) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If the last request time was more than 5 minutes ago, throw an error
    if (!checkLastRequestTime(user.lastRequestTime)) {
      return {
        status: httpStatus.UNAUTHORIZED,
        message: "Unauthorized",
        error: true,
      };
    }

    // If operation is not proxy, throw an error
    if (operation !== "proxy") {
        return {
          status: httpStatus.BAD_REQUEST,
          message: "Bad request",
          error: true,
        };
      }
 
      
    // Making the query to check service name in appProxies 
    const query = {
      [`appProxies.${service}`]: { $exists: true },
    };

    let appSettings = await AppSetting.findOne(query);

    // If the appProxies is not found, return an error
    if (!appSettings) {
      return {
        status: httpStatus.BAD_REQUEST,
        message: "Bad request",
        error: true,
      };
    }

   // Updating appProxies data by service
    appSettings.set("appProxies." + service, updatedData);

    // Update the last request time for the user
    user.lastRequestTime = microtime.nowDouble();

    await user.save();
    await appSettings.save();

    // Return a success message
    return {
      status: httpStatus.OK,
      message: "Success",
    };
  } catch (error) {
    Sentry.captureException(error);
    console.error(error.message);
    throw new Error("Failed to update appSettings by operation");
  }
};

module.exports = {
  getAllAppSettings,
  getAppSettingsByOperation,
  updateAppSettingsByOperation,
};
