const rateLimit = require("express-rate-limit");

const rateLimiter = (options) => {
  const { windowInMinutes, maxRequests, message } = options;
  return rateLimit({
    windowMs: windowInMinutes * 60 * 1000 || 15 * 60 * 1000,
    max: maxRequests || 100,
    handler: (req, res) => {
      res.status(429).json({
        success: false,
        message:
          message ||
          "You have made too many requests in a small window, please try again later.",
      });
    },
  });
};

module.exports = rateLimiter;
