
const passport = require("passport");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError"); 

const verifyCallback = (req, resolve, reject) => async (err, user, info) => {
  if (err || info || !user) {
    return reject(new ApiError(httpStatus.UNAUTHORIZED, "Unauthorized", true));
  }
  req.user = user;

  resolve();
}; 

const auth = () => async (req, res, next) => { 
  return new Promise((resolve, reject) => {
    passport.authenticate(
      'jwt',
      { session: false },
      verifyCallback(req, resolve, reject)
    )(req, res, next);
  })
    .then(() => next())
    .catch((err) => {
      const status = err.statusCode || httpStatus.INTERNAL_SERVER_ERROR;
      const message = err.message || "Something went wrong";
      res.status(status).json({ status, message });
    });
};

module.exports = auth;
