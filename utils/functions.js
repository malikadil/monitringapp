const microtime = require("microtime");

const checkLastRequestTime = (lastTime) =>{  
    // return true; 
    if(lastTime == null) return false;
    // Calculate MAXIMUM_INACTIVITY_MINUTES minutes ago in microseconds 
    const MAXIMUM_INACTIVITY_MINUTES = process.env.MAXIMUM_INACTIVITY_MINUTES;  
    // Convert last request time to microseconds
    return  microtime.now() - lastTime * 1000000 < MAXIMUM_INACTIVITY_MINUTES * 60 * 1000000 ;
}

module.exports = {
    checkLastRequestTime
}