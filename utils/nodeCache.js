const NodeCache = require("node-cache")

const cache = new NodeCache();

const KEY_SECRET = "JWT_SECRET_KEY";
const ttl = process.env.JWT_CACHE_LIFETIME_SECONDS;

const setSecretKey = (value) => {
    cache.set(KEY_SECRET, value, ttl);
}

const getSecretKey = () => {
    return cache.get(KEY_SECRET);
}

module.exports = {
    setSecretKey,
    getSecretKey
}