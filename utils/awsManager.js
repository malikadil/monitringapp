const { SecretsManagerClient, GetSecretValueCommand } = require("@aws-sdk/client-secrets-manager"); 
const Sentry = require("@sentry/node");
// Define a function to retrieve the JWT secret from AWS Secrets Manager
const getJwtSecret = async () => {
  const client = new SecretsManagerClient({
    credentials:{
      accessKeyId: process.env.APP_AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.APP_AWS_SECRET_ACCESS_KEY
    },
    region: process.env.APP_AWS_REGION
  });
  try {
    const command = new GetSecretValueCommand({ SecretId: process.env.APP_AWS_SECRET_NAME });
    const data = await client.send(command);
    const secret = JSON.parse(data.SecretString);
    return secret.JWT_SECRET_KEY;
  } catch (err) {
    Sentry.captureException(error);
    console.error(err)
    throw new Error('Error retrieving value from AWS Secrets Manager');
  } 
};

module.exports = {
    getJwtSecret
}